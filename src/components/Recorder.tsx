import React from "react";
import ReactTooltip from "react-tooltip";
import { getUserMedia } from "../utilitiies/AudioHandlers";
import { calculateSeconds } from "../utilitiies/TimerHelpers";
import { ReactComponent as Record } from "../img/mic.svg";
import { ReactComponent as Stop } from "../img/stop.svg";
import { ReactComponent as Play } from "../img/play.svg";
import { ReactComponent as Pause } from "../img/pause.svg";
import "./Recorder.css";

interface RecorderState {
  isRecording: boolean;
  isPlaying: boolean;
  timer: number;
  recordedDataChunks: Blob[];
  stream?: MediaStream;
}

export class Recorder extends React.Component<{}, RecorderState> {
  private audioRef = React.createRef<HTMLAudioElement>();
  private audioContext = new AudioContext();
  private mediaRecorder!: MediaRecorder;
  timerInterval!: number | undefined;
  width = 170;
  height = 170;

  constructor(props: any) {
    super(props);

    this.state = {
      isRecording: false,
      isPlaying: false,
      timer: 0,
      recordedDataChunks: [],
    };
  }

  componentWillUnmount() {
    this.pauseTimer();
  }

  startTimer() {
    if (!this.timerInterval) {
      this.timerInterval = window.setInterval(() => {
        this.setState((state) => {
          return { ...state, timer: state.timer + 100 };
        });
      }, 100);
    }
  }

  resetTimer() {
    this.setState({ timer: 0 });
  }

  pauseTimer() {
    if (this.timerInterval) {
      window.clearInterval(this.timerInterval);
      this.timerInterval = undefined;
    }
  }

  calculateSeconds = () => calculateSeconds(this.state.timer);

  toggleRecording = () => {
    if (this.state.isRecording) {
      this.stopRecording();
    } else {
      this.audioContext.resume();
      this.startRecording();
    }
  };

  stopRecording() {
    if (this.state.stream) {
      this.state.stream.getTracks().forEach((track) => track.stop());
      this.mediaRecorder.stop();

      this.pauseTimer();
      this.setState({ isRecording: false });
    }
  }

  startRecording() {
    this.resetDataArray();
    getUserMedia()?.then((stream) => {
      if (stream) {
        this.setState({ stream });

        this.mediaRecorder = new MediaRecorder(stream, {
          mimeType: "audio/webm",
        });

        this.mediaRecorder.ondataavailable = this.dataAvailableHandler;
        // We want to record only up to 5 seconds
        this.mediaRecorder.start(5000);

        this.setState({ isRecording: true, timer: 0 });
        this.startTimer();
      }
    });
  }

  dataAvailableHandler = (event: BlobEvent) => {
    if (event.data.size > 0) {
      this.setState({
        recordedDataChunks: [...this.state.recordedDataChunks, event.data],
      });

      if (this.mediaRecorder.state === "recording") {
        this.stopRecording();
      }
    }
  };

  togglePlay = () => {
    if (!this.state.isPlaying && this.state.recordedDataChunks.length > 0) {
      this.startPlayback();
    } else {
      this.stopPlayback();
    }
  };

  startPlayback() {
    const superBuffer = new Blob(this.state.recordedDataChunks);

    if (this.audioRef.current) {
      this.audioRef.current.src = window.URL.createObjectURL(superBuffer);
      this.audioRef.current.play();

      this.state.timer !== 0 && this.setState({timer: 0});
      this.startTimer();
      this.setState({ isPlaying: true });

      this.audioRef.current.onended = this.playbackEndedHandler;
    }
  }

  stopPlayback() {
    this.audioRef.current?.pause();
    this.setState({ isPlaying: false });
    this.pauseTimer();
  }

  playbackEndedHandler = () => {
    if (this.audioRef.current) {
      this.stopPlayback();
      this.audioRef.current.currentTime = 0;
      this.setState({timer: 0});
    }
  };

  resetDataArray() {
    this.setState({ recordedDataChunks: [] });
  }

  resetRecorder = () => {
    this.pauseTimer();
    this.setState({
      recordedDataChunks: [],
      isPlaying: false,
      isRecording: false,
      timer: 0
    });
  };

  render() {
    return (
      <div className="recorder-container">
        <div className="recorder-header">
          <h1>Audio Recorder</h1>
          <p>Record an audio clip of up to five seconds from your microphone</p>
        </div>
        <audio ref={this.audioRef} />
        <div className="recorder-timer">
          {this.calculateSeconds()}
        </div>
        <div className="recorder-button-container">
          <button
            onClick={this.toggleRecording}
            className="recorder-button"
            data-tip={
              this.state.isRecording ? "Stop recording" : "Start recording"
            }
          >
            {this.state.isRecording ? (
              <Stop height={this.height + 50} width={this.width + 50} />
            ) : (
              <Record
                height={this.height + 50}
                width={this.width + 50}
                fill="firebrick"
              />
            )}
          </button>
          <button
            disabled={this.state.isRecording}
            onClick={this.togglePlay}
            className="playback-button recorder-button"
            data-tip={
              this.state.isPlaying ? "Pause playback" : "Start playback"
            }
          >
            {this.state.isPlaying ? (
              <Pause height={this.height} width={this.width} />
            ) : (
              <Play height={this.height} width={this.width} />
            )}
          </button>
        </div>
        <ReactTooltip />
        <button
          className="reset-button"
          onClick={this.resetRecorder}
          data-tip="Reset recorder to default state"
        >
          Reset Recorder
        </button>
      </div>
    );
  }
}
