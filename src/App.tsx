import React from "react";
import "./App.css";
import { Recorder } from "./components/Recorder";

function App() {
  return (
    <div className="App">
      <main>
        <Recorder></Recorder>
      </main>
    </div>
  );
}

export default App;
