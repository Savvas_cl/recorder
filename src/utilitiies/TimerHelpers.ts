export function calculateSeconds(miliSeconds: number): string {
  const seconds = miliSeconds / 1000;

  return (
    "0" + (miliSeconds > 999 ? seconds.toPrecision(3) : seconds.toPrecision(2))
  );
}
