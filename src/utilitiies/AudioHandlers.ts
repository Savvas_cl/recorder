export function getUserMedia(): Promise<void | MediaStream> | undefined {
  if (navigator.mediaDevices) {
    return navigator.mediaDevices
      .getUserMedia({ audio: true, video: false })
      .then((stream) => stream)
      .catch((e) => {
        console.log("There was an error with your audio input device: " + e);
      });
  } else {
    console.log("getUserMedia not supported in your browser");
    return;
  }
}
