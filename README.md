This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## A simple audio recorder interface

A simple audio recorder web interface using React. It can be used to record a 5 seconds audio clip from the users microphone.
The user can also playback the last recorded audio clip. Finally a reset button sets the audio recorder to its default state.

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.
